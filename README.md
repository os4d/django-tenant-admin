django-tenant-admin
===================

Extend the Django admin with multi-tenancy capabilities.

Rationale:
1. A django model must represent the tenants (Office in the demoapp)
2. A separate set of admin classes must be configured. 
   Such classes must inherit from TenantModelAdmin with the exception eventually of the tenant admin
   class that must instead extend from MainTenantModelAdmin. (see tests/demo/demoapp/tenant_admin.py)
   The relative model must be referenced in a _model_ attribute.
   The automatic filtering rule must be specified in a field _tenant_filter_field_ using a 
   django-ORM syntax string (eg. tenant_filter_field = "office__id")
3. Users must be assigned to the tenants via a many-to-many relationship qualified by the _Group_
   (see tests.demo.demoapp.models.UserRole)
4. Do not forget to enable _staff_ flag for the admin tenants and to assign the permissions to the
   groups they are linked to the tenants through.

The strategy for linking the users to the tenants can be customized by referencing a class that
extends _tenant_admin.strategy.BaseTenantStrategy_ in the _settings.TENANT_STRATEGY_ (see 
tests.demo.demoapp.tenant_strategy.DemoStrategy)


Quick Start
===========

In the settings.py:
1. Add "tenant_admin.apps.Config" to your INSTALLED_APPS
2. Add TENANT_TENANT_MODEL, TENANT_STRATEGY, TENANT_AUTH in your settings (see tests.demoapp.settings.py)

#### settings.py
    INSTALLED_APPS = [
        "demoapp.apps.Config",
        ...
        # 'django.contrib.admin',
        ...
        "django.contrib.staticfiles",
        "smart_admin.apps.SmartTemplateConfig",
        "smart_admin.apps.SmartConfig",
        "admin_extra_buttons",
        "tenant_admin.apps.Config",
    ]

    TENANT_TENANT_MODEL = "<app_label>.<model_name>"
    TENANT_STRATEGY = "<app_label>.YourStrategy"
    TENANT_AUTH = "<backends.module>.UserRoleAuth"

#### urls.py
    import tenant_admin.sites


    urlpatterns = (
        path("manage/", tenant_admin.sites.site.urls),
        ...
    )


Some clarification:

    `Strategy` is about visibility
    `Permission` is about authorization


### Contributing
    
    git checkout https://gitlab.com/os4d/django-tenant-admin.git
    cd django-tenant-admin
    python -m venv .venv
    . .venv/bin/activate
    pip install -e .[dev]
    pre-commit install

### Create sample data

This will create the DB ./django_tenant_admin.sqlite and populate with some example data.

    ./manage.py demo

This will create:
- 4 `Country` records: 'Afghanistan', 'Ukraine', 'Somalia', 'Sudan'
- 4 `Office` records: 'Afghanistan Office', 'Ukraine Office', 'Somalia Office', 'Sudan Office'
- 10 `Employee` records: 'Employee 0' to 'Employee 9'
- An 'admin-ukr' user assigned to 'Ukraine Office'
- An 'admin-afg' user and 8 additional users (_username3_ to _username12_) assigned to 'Afghanistan Office'
- An 'admin' user not assigned to any office


Try it with:

    ./manage.py runserver

Connect to http://localhost:8000

NB:
- No need to create users as for demo purposes there is a backend that will create the user on the fly using any
password



