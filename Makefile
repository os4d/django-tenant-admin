BUILDDIR=${PWD}/~build

.mkbuilddir:
	@mkdir -p ${BUILDDIR}

define PRINT_HELP_PYSCRIPT
import re, sys

for line in sys.stdin:
	match = re.match(r'^([a-zA-Z0-9_-]+):.*?## (.*)$$', line)
	if match:
		target, help = match.groups()
		print("%-20s %s" % (target, help))
endef
export PRINT_HELP_PYSCRIPT

help:
	@python -c "$$PRINT_HELP_PYSCRIPT" < $(MAKEFILE_LIST)

lint:  ## run flake8
	flake8 src/ tests/

mypy:
	mypy --python-executable=${PWD}/.venv/bin/python \
		src/

develop: ## Create virtualenv and setup development environment.
	python3 -m venv ./.venv
	./.venv/bin/pip install -e .[dev]

clean:  ## clean development environment
	rm -fr ${BUILDDIR} dist build *.egg-info .coverage coverage.xml pytest.xml .cache MANIFEST ~build .pytest_cache
	find . -name __pycache__ | xargs rm -rf
	find . -name "*.py?" -o -name "*.orig" -prune | xargs rm -rf

fullclean: ## clean development environment plus removes virtualenv an Tox working dir
	rm -fr .tox .cache
	rm -fr *.sqlite
	$(MAKE) clean

