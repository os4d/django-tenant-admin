from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin

from tenant_admin.options import MainTenantModelAdmin, TenantModelAdmin
from tenant_admin.skeleton import Skeleton

from . import admin as skeleton_admin
from .admin import LeaveAdmin
from .models import Employee, Leave, Office


class OfficeTenantAdmin(MainTenantModelAdmin):
    tenant_filter_field = "id"
    model = Office


class EmployeeTenantAdmin(TenantModelAdmin):
    tenant_filter_field = "office__id"
    model = Employee
    skeleton = Skeleton(skeleton_admin.EmployeeAdmin)
    writeable_fields = [
        "name",
    ]


class UserAdmin(TenantModelAdmin):
    skeleton = BaseUserAdmin
    model = get_user_model()
    tenant_filter_field = "roles__office__id"


class LeaveTenantAdmin(TenantModelAdmin):
    skeleton = LeaveAdmin
    model = Leave
    tenant_filter_field = "employee__office__id"
