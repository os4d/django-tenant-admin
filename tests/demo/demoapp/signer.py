from django.core.signing import Signer


class DummySigner(Signer):
    def sign(self, value):
        return "%s" % value

    def unsign(self, value):
        return "%s" % value
