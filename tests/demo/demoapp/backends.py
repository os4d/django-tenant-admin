from demoapp.models import UserRole
from django.conf import settings
from django.contrib.auth.backends import ModelBackend
from django.contrib.auth.models import User

from tenant_admin.auth import BaseTenantAuth


class AnyUserAuthBackend(ModelBackend):
    def authenticate(self, request, username=None, password=None, **kwargs):
        if username and settings.DEBUG:
            defaults = dict(
                is_staff=True,
                is_active=True,
                is_superuser=username in ["admin", "sax"],
                email=f"{username}@demo.org",
            )
            user, __ = User.objects.update_or_create(
                username=username, defaults=defaults
            )
            # if user:
            #     UserRole.objects.get_or_create(user=user,
            #                                    group=Group.objects.first(),
            #                                    office=Office.objects.first(),
            #                                    )
            return user


class UserRoleAuth(BaseTenantAuth):
    model = UserRole
