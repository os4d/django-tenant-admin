from unittest.mock import Mock


def test_auth(db):
    from demoapp.factories import UserRoleFactory
    from demoapp.models import UserRole

    from tenant_admin.auth import BaseTenantAuth

    office_role = UserRoleFactory()
    s = BaseTenantAuth()
    s.model = UserRole
    assert (
        s.get_allowed_tenants(Mock(user=office_role.user)).first() == office_role.office
    )
