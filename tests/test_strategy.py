import pytest
from django.http import HttpResponse

from tenant_admin.config import conf


@pytest.fixture()
def strategy():
    from tenant_admin.strategy import BaseTenantStrategy

    return BaseTenantStrategy(conf)


def test_set_selected_tenant(strategy, afghanistan, settings):
    settings.SIGNING_BACKEND = "demoapp.signer.DummySigner"

    response = HttpResponse()
    strategy.set_selected_tenant(response, afghanistan)
    assert response.cookies[conf.COOKIE_NAME].value == str(afghanistan.pk)


def test_get_selected_tenant(strategy, afghanistan, rf, settings):
    from demoapp.models import Office

    settings.SIGNING_BACKEND = "demoapp.signer.DummySigner"
    settings.TENANT_COOKIE_NAME = "test_cookie"
    strategy.config.auth.get_allowed_tenants = lambda x: Office.objects.all()
    request = rf.get("/")
    request.COOKIES = {"test_cookie": str(afghanistan.pk)}
    ret = strategy.get_selected_tenant(request)
    assert ret == afghanistan


def test_get_selected_tenant_fail(strategy, rf, settings):
    from demoapp.models import Office

    settings.SIGNING_BACKEND = "demoapp.signer.DummySigner"
    settings.TENANT_COOKIE_NAME = "test_cookie"
    strategy.config.auth.get_allowed_tenants = lambda x: Office.objects.all()
    request = rf.get("/")
    request.COOKIES = {"test_cookie": "-22"}
    ret = strategy.get_selected_tenant(request)
    assert ret is None
