from unittest.mock import Mock

from tenant_admin.utils import set_current_tenant


def test_perms(user):
    from demoapp.models import UserRole

    from tenant_admin.auth import BaseTenantAuth

    b = BaseTenantAuth()
    b.model = UserRole
    from demoapp.factories import OfficeFactory

    o = OfficeFactory()
    with set_current_tenant(o):
        assert b.get_all_permissions(Mock(user=user)) == set()
