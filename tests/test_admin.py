import pytest
from django.urls import reverse

from tenant_admin.config import conf


@pytest.fixture()
def employees(db):
    from demoapp.factories import EmployeeFactory

    EmployeeFactory.create_batch(10, office__country__name="Afghanistan")


@pytest.fixture()
def employee(db):
    from demoapp.factories import EmployeeFactory

    return EmployeeFactory(office__country__name="Afghanistan")


@pytest.mark.django_db
def test_changelist(app, tenant_user, employees, settings):
    settings.SIGNING_BACKEND = "demoapp.signer.DummySigner"
    url = reverse("tenant_admin:demoapp_employee_changelist")
    app.set_cookie(conf.COOKIE_NAME, "1")
    res = app.get(url, user=tenant_user.username)
    assert "Employee 0" in str(res.content)


@pytest.mark.django_db
def test_change(app, tenant_user, employee, settings):
    settings.SIGNING_BACKEND = "demoapp.signer.DummySigner"
    url = reverse("tenant_admin:demoapp_employee_change", args=[employee.pk])
    app.set_cookie(conf.COOKIE_NAME, "1")

    res = app.get(url, user=tenant_user.username)
    res.forms["employee_form"]["name"] = "Employee #00"
    res = res.forms["employee_form"].submit().follow()
    assert "Employee #00" in str(res.content)


@pytest.mark.django_db
def test_add(app, tenant_user, settings):
    settings.SIGNING_BACKEND = "demoapp.signer.DummySigner"
    url = reverse("tenant_admin:demoapp_employee_add")
    app.set_cookie(conf.COOKIE_NAME, "1")

    res = app.get(url, user=tenant_user.username)
    res.forms["employee_form"]["name"] = "Employee New"
    res = res.forms["employee_form"].submit().follow()
    assert "Employee New" in str(res.content)
